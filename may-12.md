# May 12 Technical Interview

## A word of advice
In Academy, we expect you to know how to read requirements and how to code in your language.<br>

To prove this, we ask you to implement this task and submit it via Gitlab to confirm that you are ready to study at Academy.<br>
This task requires approximately two working hours to solve. It does not mean that you will solve it in two hours. It is ok to spend up to four hours on this task.<br>
We suggest you spend at least 40 minutes reading the task and modeling the solution. We tested this task on our engineers: several Syberry Junior Developers spent 40 minutes reading and thinking and approximately 1:30 hours of programming it on average.<br>

### Task Deadline: 15:35
We start a countdown from the moment we end the Zoom call.<br>
**We accept solutions before Wednesday, 15:35 Minsk Time (GMT+3)**

# Rover v02
Update your [first version of Rover code](https://gitlab.com/SyberryAcademy/syberry-academy-e07-test-task).<br>
Your task is to calculate the path with minimized fuel cost.<br>

The first version is working, but real-life tests showed that it didn't match the reality.

## What are the changes?

### Below the sea level
The previous version processes only the terrain that is above sea level. But in reality, the landscape can be both above and below sea level. The new version of the code must handle different terrains.

The numbers still show the height. Zero 0 is a sea level. Positive numbers show the elevation above sea level. Negative numbers mean that the terrain is below sea level.

For example, here is already parsed photo of a small lake:<br>
{{"0","-1","-1","-1","0"}, <br>
{"-1","-1","-3","-1","-1"}, <br>
{"0","-1","-1","-1","0"}, <br>
{"0","0","0","0","0"}} <br>

### Impossible Elevation
Nature is unpredictable, and sometimes there are places that the Rover cannot reach. Such terrain is marked as X on the photo. Rover cannot go into that place.<br>
For example, here is a unparsed photo with unreachable terrain:<br>
 1 1 X X X <br>
 1 1 X X 8 <br>
 1 1 0 0 3 <br>

### Updated movement
Now your Rover can move diagonally! It still cannot get back to the same place, though.

Rover still moves from the [0][0] to [N - 1][M - 1]. N and M are arbitrary positive numbers. 

### Updated fuel mileage
#### Fuel Mileage with Negative Numbers
The fuel cost works the same with negative numbers: moving from 0 to 2 will cost the same two fuel units as moving from 0 to -2. Moving from 2 to -2 will cost the same as moving from 4 to 0.

#### Fuel Mileage with Diagonal Movement
Diagonal movement requires different fuel mileage. Every second diagonal move consumes two fuel units. The first diagonal move is one fuel, the second diagonal move is two fuel, the third is one fuel, the fourth is two, etc.

For example, here  <br>
 1 2 1 <br>
 1 2 1 <br>
 1 7 0 <br>
a path from [0][0] to [1][1] costs 1 fuel for diagonal move plus 1 fuel for elevation, and a path from [1][1] to [2][2] costs 2 fuel for the second diagonal move and 2 fuel for descent.

### Error handling
#### Data
Data is not ideal. Sometimes the parser that converts from the photo to numbers shows bizarre results. Please make sure that the matrix contains only numerals and the 'X' sign.

#### Exceptions
Something may go wrong. There may be no matrix at all, or the matrix may contain weird data, or the path may start with X at [0][0]. There are tons of ways that the program can go wrong.

Implement exception handling. The exception rules: if the Rover cannot start its movement, throw the CannotStartMovement exception. End the program and write the reason to path-plan.txt

So, if the Rover cannot move, throw an exception and end the program. Write to the path-plan.txt something like "Cannot start a movement because ...... ." Come up with your description of a problem. Write in clear and simple English.

### String parsing
The input matrix now is a string array, not an integer array. 

Input example:<br>
Java and C#:<br>
{{"1", "2"},{"1","X"},{"0","1"}}

JS:<br>
[['1', '2'],['1','X'],['0','1']]

## Requirements
Update the calculateRoverPath(map) function. 

The function must take a two-dimensional string array as input<br>
The result of the program run is a path-plan.txt file stored in the same folder where the file with code is

#### Java
Class `Rover`<br>
The function `calculateRoverPath` must be static<br>
Do not add packages<br>
Java Version: 14 and older<br>
Do not use `System.out.*` or any other output methods<br>

`public static void calculateRoverPath(String[][] map)`

#### C#
Class `Rover`<br>
The function `calculateRoverPath` must be static<br>
Do not use namespace<br>
C# version 8+<br>
Do not use `Console.Write.*` or any other output methods

`public static void CalculateRoverPath(string[,] map)`

#### JS
Do not use `console.log` or any other output methods <br>
Add `module.exports` at the end of your file<br>
```
module.exports = {
    calculateRoverPath,
};
```

`function calculateRoverPath(map)`


## Restrictions
Do not use libraries for algorithms. You must implement the algorithm using the basic instruments of your language. <br>
You may use these libraries for writing to file. Use the most straightforward ways possible.<br>
JS: you must only use [Node js](https://nodejs.org/en/knowledge/file-system/how-to-write-files-in-nodejs/) for writing to file.<br>

### Code Style
**You `MUST` follow the coding style guide for your language.** <br>
[C# Style Guide](https://google.github.io/styleguide/csharp-style.html) <br>
[Java Style Guide](https://google.github.io/styleguide/javaguide.html) <br>
[JS Style Guide](https://github.com/airbnb/javascript) <br>
Do not use Russian language in your code or comments. Write only in English <br>

# How to submit
Create your personal **private** repository in GitLab. Name it `Name-Surname-Academy-Technical-Interview`. It is important to have your repository **PRIVATE**. We won't check solutions in the public repository. <br>
The end time is the time of your last commit in your repository.<br>
Submit a file named `Rover.__`and a blank README.md<br>
A file `MUST` contain the function `calculateRoverPath(map)`. You may add any other functionality to the file. <br>
We'll need read permission to your repository to check your solution. Add user SyberryAcademy to your repository as a Maintainer.<br>
Send a link to your repository with the solved task to academy@syberry.com. Email subject: "Syberry Academy Technical Interview Name Surname". For example, "Syberry Academy Technical Interview Jane Doe".<br>
We have an authomatic filter on our mailbox. If you won't write a subject as we asked, we won't see your task and disqualify your solution.

# What's next
If you did well on our task, our Recruiter will call you and invite you to an HR interview. If not, we'll send you an email with your results and comments.

We will need five working days to process your results.

# Sanity Check: Please Double Check these Requirements
The following requirements `MUST` be completed. If not, we won't check your task.

- The Rover.__ file `MUST` in your Gitlab repository in the root folder (see example)
- Do not use output methods
- Do not use packages or namespace
- The repository `MUST` be private
- User SyberryAcademy `MUST` be added as a Maintainer
- The email has a proper subject
- The last commit is within 4 hours, the email is sent within 5 hours
- Your `main()` function does not have test data (or any other data)
- Your repository has the following structure:
```
Jane-Doe-Academy-Technical-Interview
    Rover.cs
    README.md
```

```
Keanu-Reeves-Technical-Interview
    Rover.java
    README.md
```

```
Henry-Cavill-Technical-Interview
    rover.js
    README.md
```

No Slacking Off!
